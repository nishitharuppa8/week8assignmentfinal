<%@page import="com.hcl.beans.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h4 align="center">ReadLater BooksList </h4>
<a href="Welcome.jsp">Home Page</a>
<body> 
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>TITLE</th>
			<th>GENRE</th>
			
			
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> iterator = listOfBooks.iterator();
	while(iterator.hasNext()){
		Books book  = iterator.next();
		%>
		<tr>
			<td><%=book.getBookId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>