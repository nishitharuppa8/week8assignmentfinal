<%@page import="com.hcl.beans.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h1 align="center">WELCOME TO BOOKS STORE </h1>
<a href="logout.spring">SIGN OUT</a>
<body >
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>TITLE</th>
			<th>GENRE</th>
			
			
	</tr>
<%  
	Object user=session.getAttribute("user");
    if(user!=null){
	out.println("Welcome "+user);
	}
	Object obj = session.getAttribute("obj1");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getBookId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
			
			<td><form action="like.spring" method="post">
			<input type="hidden" name="id" value=<%=book.getBookId()%>> 
			<input type="submit"  value="Like" name="Like"></input></form></td>

			<td><form action="readlater.spring" method="post">
			<input type="hidden" name="id" value=<%=book.getBookId() %>>
			<input type="submit"  value="readlater" name="readlater"></input></form></td>
		</tr>
		<% 
	}
%>
</table>
</div>
<br>
<a href="likedbooks.spring"> LikedBooks</a><br>
<a href="readlaterbooks.spring">ReadLaterBooks</a>

</body>
</html>