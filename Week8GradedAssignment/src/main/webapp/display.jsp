<%@page import="com.hcl.beans.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h3 align="center">AVAILABLE BOOKS LIST </h3>
<a href="index.jsp">BACK</a>
<a href="welcome.spring">Login</a><br>
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>TITLE</th>
			<th>GENRE</th>
			
			
	</tr>
<% 
	Object obj = session.getAttribute("obj");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> iterator = listOfBooks.iterator();
	while(iterator.hasNext()){
		Books book  = iterator.next();
		%>
		<tr>
			<td><%=book.getBookId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>