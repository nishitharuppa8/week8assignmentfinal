package com.hcl.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.beans.Login;
import com.hcl.daos.LoginDao;

@Service
public class LoginService {
	@Autowired
    LoginDao loginDao;
	public String storeRegistrationInfo(Login login) {
		if(loginDao.StoreRegistrationDetails(login)>0) {
			System.out.println("In a service");
			return "Sucessfully Logged in";
			
		}else {
			return "Failed to login";
		}
	}
	
	public String checkLoginInfo(String email) {
		return loginDao.checkLoginDetails(email);
		
		
	}

}