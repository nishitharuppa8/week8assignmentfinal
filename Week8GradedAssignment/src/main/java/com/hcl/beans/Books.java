package com.hcl.beans;

import org.springframework.stereotype.Component;

@Component
public class Books {
	private int bookId;
	private String title;
	private String genre;
	
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
		
	@Override
	public String toString() {
		return "Books [bookId=" + bookId + ", title=" + title + ", genre=" + genre + ",  ]";
	}

}
