package com.hcl.controllers;

import java.util.Iterator;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.beans.Books;
import com.hcl.services.BooksService;

@Controller
public class BookController {
	@Autowired
	BooksService booksService;
	List<Books>listOfBooks;
	@RequestMapping(value="display",method=RequestMethod.GET)
	public ModelAndView displayAllBooks(HttpServletRequest servletReq,HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		listOfBooks=booksService.fetchAllBooks();	
		httpSession.setAttribute("obj", listOfBooks);
		mvc.setViewName("display.jsp");
		return mvc;
		
	}
	@RequestMapping(value="Dashboard",method=RequestMethod.GET)
	public ModelAndView displayBooksForUser(HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		listOfBooks=booksService.fetchAllBooks();	
		httpSession.setAttribute("obj1", listOfBooks);
		mvc.setViewName("Welcome.jsp");
		return mvc;

	}
	@RequestMapping(value="like",method=RequestMethod.POST)
	public ModelAndView listOfBooks(HttpServletRequest req,HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		if(req.getParameter("Like")!=null) {
			int id=Integer.parseInt(req.getParameter("id"));
		Iterator<Books>iterator=listOfBooks.iterator();
		while(iterator.hasNext()) {
			Books bookStore=iterator.next();
			if(id==bookStore.getBookId()) {
				Object user=httpSession.getAttribute("user");
				String result=booksService.storeLikedBooksInfo(bookStore, user.toString());
				mvc.setViewName("Welcome.jsp");
				
			}
			
		}
		}
		return mvc;		
	}
	@RequestMapping(value="readlater",method=RequestMethod.POST)
	public ModelAndView listOfReadLaterBooks(HttpServletRequest req,HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		if(req.getParameter("readlater")!=null) {
			int id=Integer.parseInt(req.getParameter("id"));
		Iterator<Books>iterator=listOfBooks.iterator();
		while(iterator.hasNext()) {
			Books bookStore=iterator.next();
			if(id==bookStore.getBookId()) {
				Object user=httpSession.getAttribute("user");
				String result=booksService.storeReadLaterBooksInfo(bookStore, user.toString());
				mvc.setViewName("Welcome.jsp");
				
			}
			
		}
		}
		return mvc;
	}
	@RequestMapping(value="likedbooks",method=RequestMethod.GET)
	public ModelAndView displayLikedBooksForUser(HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		Object user=httpSession.getAttribute("user");
		listOfBooks=booksService.fetchAllLikedBooks(user.toString());	
		httpSession.setAttribute("obj2", listOfBooks);
		System.out.println(listOfBooks);
		mvc.setViewName("likedbooks.jsp");
		return mvc;

	}
	@RequestMapping(value="readlaterbooks",method=RequestMethod.GET)
	public ModelAndView displayReadLaterBooksForUser(HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		Object user=httpSession.getAttribute("user");
		listOfBooks=booksService.fetchAllReadLikedBooks(user.toString());	
		httpSession.setAttribute("obj3", listOfBooks);
		System.out.println(listOfBooks);
		mvc.setViewName("readlater.jsp");
		return mvc;

	}
	
	
	@RequestMapping(value="logout")
	public ModelAndView logout() {
		ModelAndView mvc=new ModelAndView();
		mvc.setViewName("logout.jsp");
		return mvc;
	}
	
}