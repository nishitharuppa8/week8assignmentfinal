package com.hcl.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.beans.Login;
import com.hcl.services.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;
	@RequestMapping(value="welcome",method=RequestMethod.GET)
	public ModelAndView welcomepage() {
		ModelAndView mvc=new ModelAndView();
		mvc.setViewName("login.jsp");
		return mvc;
	}
	@RequestMapping(value="Register",method=RequestMethod.GET)
	public ModelAndView Registrationpage() {
		ModelAndView mvc=new ModelAndView();
		mvc.setViewName("registration.jsp");
		return mvc;
	}
	@RequestMapping(value="Registration",method=RequestMethod.POST)
	public ModelAndView StoreRegistrationDetails(HttpServletRequest ServletReq) {
		ModelAndView mvc=new ModelAndView();
				
		String email=ServletReq.getParameter("email");
		String pass=ServletReq.getParameter("pass");
		
		Login login=new Login();
		login.setEmail(email);
		login.setPassword(pass);
	
		String result=loginService.storeRegistrationInfo(login);
		if(result.equalsIgnoreCase("success")) {
			ServletReq.setAttribute("loginMessage","You Registered Sucessfully");
			mvc.setViewName("login.jsp");
			
		}else {
			ServletReq.setAttribute("registermsg","Registration Failed!  Try Again after Sometime");
			mvc.setViewName("index.jsp");	
		}			
		return mvc;
	}
	@RequestMapping(value="login",method=RequestMethod.GET)
	public ModelAndView checkLoginDetails(HttpServletRequest servletReq,HttpSession httpSession) {
		ModelAndView mvc=new ModelAndView();
		String email=servletReq.getParameter("email");
		String password=servletReq.getParameter("pass");
		String user=email.substring(0, email.indexOf('@'));
		Login log=new Login();
		log.setEmail(email);
		log.setPassword(password);
	
		String result=loginService.checkLoginInfo(email);
		if(result.equalsIgnoreCase(password)) {
			httpSession.setAttribute("user",user);		
			mvc.setViewName("dashboard.jsp");
		}else {
			servletReq.setAttribute("flog","Please give details properly");
			mvc.setViewName("index.jsp");
		}
		
		
		return mvc;
		
	}
	

}

