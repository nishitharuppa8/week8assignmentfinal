package com.hcl.daos;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.beans.Books;

@Repository
public class BooksDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<Books> getAllBooks(){
		try {
			return jdbcTemplate.query("select * from books",new bookRowMapper());
		} catch (Exception exp) {
			System.out.println("Restoring books"+exp);
			return null;
		}
		
		
	}
	public int storeLikedBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into likedbooks values(?,?,?)",books.getBookId(),books.getTitle(),
					books.getGenre(),user);
		} catch (Exception exp) {
			System.out.println("Liked books"+exp);
			return 0;
		}
	}
	public int storeReadLaterBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into readlaterbooks values(?,?,?)",books.getBookId(),books.getTitle(),
					books.getGenre(),user);
		} catch (Exception exp) {
			System.out.println(" Readlater books"+exp);
			return 0;
		}
	}
	public List<Books> getAllLikedBooks(String user){
		try {
			return jdbcTemplate.query("select bookId,title,genre  from likedbooks where user=?",new likedBookRowMapper(),user);
		} catch (Exception exp) {
			System.out.println(" GetlikedBooks"+exp);
			return null;
		}

}
	public List<Books> getAllReadLaterBooks(String user){
		try {
			return jdbcTemplate.query("select bookId,title,genre  from readlaterbooks where user=?",new readLaterBookRowMapper(),user);
		} catch (Exception exp) {
			System.out.println(" GetlikedBooks"+exp);
			return null;
		}

}
}
class bookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books bookRow=new Books();
		bookRow.setBookId(resultSet.getInt(1));
		bookRow.setTitle(resultSet.getString(2));
		bookRow.setGenre(resultSet.getString(3));
	
		return bookRow;
	}
	
}
class likedBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books bookList=new Books();
		bookList.setBookId(resultSet.getInt(1));
		bookList.setTitle(resultSet.getString(2));
		bookList.setGenre(resultSet.getString(3));
		return bookList;
	}
	
}
class readLaterBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Books readLater=new Books();
		readLater.setBookId(resultSet.getInt(1));
		readLater.setTitle(resultSet.getString(2));
		readLater.setGenre(resultSet.getString(3));
		return readLater;
	}
	
}


